﻿#include<iostream>
#include<fstream>
#include<string>
using namespace std;
#include<iomanip>

class DaThuc
{
private:
	struct DonThuc//Là đơn thức có dạng a*x^n
	{

		float a;
		unsigned int x;
		unsigned int n;
	};
	struct PhanTu
	{
		DonThuc data;
		PhanTu *Next;
		PhanTu* ThanhPhan;// Lưu phần tử đơn thức chỉ mang giá trị biến và số mũ (Dùng trong phép nhân) nếu có, khi được khởi tạo đa thức, giá trị này bằng NULL.
	};
	PhanTu * Head;
	bool comparePhanTu(PhanTu*p, PhanTu*q);
public:
	DaThuc()
	{
		Head = NULL;
	}
	void Input(char *tenfile);
	void Output(char *tenfile, DaThuc a);
	void RutGon();
	void ChuanHoa();
	friend DaThuc operator+(DaThuc a, DaThuc b);
	friend DaThuc operator-(DaThuc a, DaThuc b);
	DaThuc operator*(DaThuc);
	void xuly(string str);
	void InSert( DaThuc::PhanTu* newdata);
	PhanTu* Sreach_Tail();
	friend PhanTu* Creat_Node(DonThuc key);
	void init();
	friend ostream& operator<<(ostream& os,  DaThuc& p);
	friend ostream& operator<<(ostream& os, DaThuc::PhanTu* & p);
	friend ostream& operator<<(ostream& os, DaThuc::DonThuc& p);

};
string xulychuoi(string str);
void DaThuc::Input(char *tenfile)
{
	fstream file;
	file.open(tenfile, ios::in);
	if (!file)
	{
		perror("Error: Not open file");
		exit(1);
	}
	string str;
	getline(file, str);
	xuly(str);
	file.close();
}

string xulychuoi(string str)
{
	string temp;
	for (int i = 0; i < str.length(); i++)
	{
		if (str[i] == '*' || str[i] == '^' || str[i] == '+' || str[i] == '-')
		{
			temp.push_back(' ');
			temp.push_back(str[i]);
			temp.push_back(' ');
		}
		else
			temp.push_back(str[i]);
	}
	return temp;
}



void DaThuc::xuly(string str)
{
	str = xulychuoi(str);

	char* sep = " \n";
	string temp = str;
	char* word = strtok((char*)temp.c_str(), sep);
	while (word != NULL)
	{
		int kt = 1; //1 : dương  | 0 : âm
		DonThuc p;
		if (strcmp(word, "+") == 0 || strcmp(word, "-") == 0)
		{
			if (strcmp(word, "-") == 0) kt = 0;
			word = strtok(NULL, sep);
		}
		p.a = atof(word);
		if (kt == 0) p.a = -(p.a);
		word = strtok(NULL, sep);
		if (word != NULL && strcmp(word, "*") == 0)
		{
			word = strtok(NULL, sep);
			p.x = atoi(word);
			word = strtok(NULL, sep);
			if (word != NULL && strcmp(word, "^") == 0)
			{
				word = strtok(NULL, sep);
				p.n = atoi(word);
			}
			else
				p.n = 1;

			if (word != NULL && strcmp(word, "+") != 0 && strcmp(word, "-") != 0)
				word = strtok(NULL, sep);
		}
		else
		{
			p.x = p.n = 1;
		}
		//if (p.a != 0 && p.x != 0) Loại đi để lưu tất cả các thông tin từ file .txt
			InSert(Creat_Node(p));
	}
}

DaThuc::PhanTu* Creat_Node(DaThuc::DonThuc key)
{
	DaThuc::PhanTu* p = new DaThuc::PhanTu;
	p->data = key;
	p->Next = NULL;
	p->ThanhPhan = NULL;
	return p;
}



DaThuc::PhanTu* DaThuc::Sreach_Tail()
{
	for (DaThuc::PhanTu * i = Head; i != NULL; i = i->Next)
	{
		if (i->Next == NULL)return i;
	}
	return NULL;
}

void DaThuc::InSert(PhanTu* newdata)
{
	if (Head == NULL)
	{
		Head = newdata;
	}
	else
	{
		DaThuc::PhanTu * p = Sreach_Tail();
		p->Next = newdata;
	}
}

void DaThuc::ChuanHoa()
{
	PhanTu *q = Head;
	while (q->Next != NULL)
	{
		PhanTu *p = q->Next;
		while (p != NULL)
		{
			if (q->data.n >= p->data.n)
			{
				if (q->data.n == p->data.n)
				{
					if (q->data.x <= p->data.x)
					{
						p = p->Next;
						continue;
					}
				}
				PhanTu *tam = new PhanTu;
				tam->data = q->data;
				tam->ThanhPhan = q->ThanhPhan;
				q->data = p->data;
				q->ThanhPhan = p->ThanhPhan;
				p->data = tam->data;
				p->ThanhPhan = tam->ThanhPhan;
			}
			p = p->Next;
		}
		q = q->Next;
		if (q == NULL) break;
	}
}

bool DaThuc::comparePhanTu(PhanTu*p, PhanTu*q)
{
	if (p->ThanhPhan != NULL&&q->ThanhPhan != NULL) {// Nếu cả hai đều có thành phần
		//kiểm tra xem PhanTu chính của q,p và phần tử thành phần của q,p có thể rút gọn không
		if (p->data.x == q->data.x&&p->data.n == q->data.n&&
			p->ThanhPhan->data.x == q->ThanhPhan->data.x&&p->ThanhPhan->data.n == q->ThanhPhan->data.n) return true;
		//Kiểm tra xem phanTu chính của q có rút được với phần tử thành phần của p không và
		//_____________________________ p ______________________________________q_______
		if (p->data.x == q->ThanhPhan->data.x&&p->ThanhPhan->data.n == q->data.n&&
			p->ThanhPhan->data.x == q->data.x&&p->ThanhPhan->data.n == q->data.n) return true;
	}
	else {
		if (p->ThanhPhan == NULL&&q->ThanhPhan == NULL) {// Nếu cả 2 đều k có thành phần
			if (p->data.x == q->data.x&&p->data.n == q->data.n) return true;
		}
		else {//Một trong 2 có
			return false;
		}
	}
	return false;
}

void DaThuc::RutGon()
{
	if (Head == NULL) return; // DaThuc rỗng thì thoát
	PhanTu*Temp; // biến lưu PhanTu cần xóa để giải phóng bộ nhớ
	//Xóa những phần tử đầu có hệ số =0
	while (Head->data.a == 0||Head->data.n == 0) {
		Temp = Head;
		Head = Head->Next;
		delete Temp;
	}

	PhanTu *p = Head;

	//Tiến hành rút gọn hệ số
	while (p != NULL)
	{
		PhanTu *q = p->Next;
		PhanTu *a = p;
		while (q != NULL)
		{
			//Các phần tử khác head có hệ số =0 thì xóa và kết thúc vòng lặp
			if (q->data.a == 0||q->data.x == 0)
			{
				Temp = q;
				a->Next = q->Next;
				q = q->Next;
				delete Temp;
				continue;
			}
			if (comparePhanTu(p, q) == true)// nếu p và q có phần khác hệ số a mà bằng nhau
			{
				p->data.a = p->data.a + q->data.a;
				Temp = q;
				a->Next = q->Next;
				q = q->Next;
				delete Temp;
				continue;
			}
			q = q->Next;
			a = a->Next;
		}
		p = p->Next;
	}
}

DaThuc DaThuc::operator *(DaThuc Buffer)
{
	PhanTu *Temp1 = this->Head;
	DaThuc KQ;
	// Temp1 là phần tử chạy duyệt đa thức 1 
	// Temp2 là phần tử chạy duyệt đa thức 2
	// Temp3 là biến phần tử tạo ra để thêm vào Đa thức KQ
	while (Temp1 != NULL)
	{
		PhanTu*Temp2 = Buffer.Head;
		while (Temp2 != NULL)
		{
			// Nếu có cùng giá trị biến x, thì chỉ cần cộng thêm n và nhân hệ số
			if (Temp1->data.x == Temp2->data.x)
			{
				PhanTu *Temp3 = new PhanTu;
				Temp3->data = Temp1->data;
				Temp3->data.a = Temp1->data.a*Temp2->data.a;
			}
			else
			{
				PhanTu *Temp3 = new PhanTu;
				// Nếu như biến x khác nhau, thì nhân hệ số a và thêm PT con vào các TP tương ứng
				// Kiểm tra xem số mũ nào lớn cho ra trước, sô mũ nhỏ cho làm Thành phần
				Temp3->data.a = Temp1->data.a*Temp2->data.a;
				if (Temp1->data.n > Temp2->data.n)
				{
					Temp3->data.n = Temp1->data.n;
					Temp3->data.x = Temp1->data.x;
					PhanTu *TempTP = new PhanTu;
					TempTP->data = Temp2->data;
					TempTP->data.a = 1;
					TempTP->Next = NULL;
					TempTP->ThanhPhan = NULL;
					Temp3->Next = TempTP;
				}
				else
				{
					Temp3->data.n = Temp2->data.n;
					Temp3->data.x = Temp2->data.x;
					PhanTu *TempTP = new PhanTu;
					TempTP->data = Temp1->data;
					TempTP->data.a = 1;
					TempTP->Next = NULL;
					TempTP->ThanhPhan = NULL;
					Temp3->Next = TempTP;
				}
				KQ.InSert(Temp3);
			}
			Temp2 = Temp2->Next;
		}
		Temp1 = Temp1->Next;
	}
	return KQ;
}

DaThuc operator+(DaThuc a, DaThuc b)
{
	DaThuc C;
	for (DaThuc::PhanTu* i = a.Head; i != NULL; i = i->Next)
		C.InSert(Creat_Node(i->data));
	for (DaThuc::PhanTu* i = b.Head; i != NULL; i = i->Next)
		C.InSert(Creat_Node(i->data));
	return C;
}

DaThuc operator-(DaThuc a, DaThuc b)
{
	DaThuc C;
	for (DaThuc::PhanTu* i = a.Head; i != NULL; i = i->Next)
		C.InSert(Creat_Node(i->data));
	for (DaThuc::PhanTu* i = b.Head; i != NULL; i = i->Next)
	{
		DaThuc::DonThuc p = i->data;
		p.a = -p.a;
		C.InSert(Creat_Node(p));
	}

	return C;
}
void DaThuc::init()
{
	PhanTu *p = Head;
	while (p != NULL)
	{
		cout << p->data.a << "*" << p->data.x;
		if (p->data.n != 1)
			cout << "^" << p->data.n;
		p = p->Next;
		if (p != NULL && p->data.a >= 0) cout << "+";
	}
}
int check_key = 1;

ostream& operator<<(ostream& os,  DaThuc& p)
{
	check_key = 1;
	os << p.Head;
	os << endl;
	return os;
}

ostream& operator<<(ostream& os, DaThuc::DonThuc& p)
{
	if (check_key == 0)os << "";
	if (p.a > 0 && check_key == 0)
		os << "+";
	else
		check_key = 0;
	if (p.a < 0 )
		os << "-";
	os << fixed;
	if (p.a < 0)
		os << setprecision(2) << abs(p.a);
	else
	os << setprecision(2) << p.a;

	if (p.x != 1)
		os << "*" << p.x;
	if (p.n != 1)
		os << "^" << p.n;
	return os;
}
ostream& operator<<(ostream& os,  DaThuc::PhanTu* & p)
{
	
	for (DaThuc::PhanTu* i = p; i != NULL; i = i->Next)
	{
		os << i->data;
	}
	return os;
}

void DaThuc::Output(char *tenfile, DaThuc F2){
	fstream fo;
	fo.open(tenfile, ios::out);

	RutGon();
	ChuanHoa();
	F2.RutGon();
	F2.ChuanHoa();

	fo << "F1" << endl;
	fo << *this << endl;
	fo << "F2" << endl;
	fo << F2 << endl;

	DaThuc F3 = *this + F2;
	F3.RutGon();
	F3.ChuanHoa();
	fo << "F3" << endl;
	fo << F3 << endl;

	DaThuc F4 = *this - F2;
	F4.RutGon();
	F4.ChuanHoa();
	fo << "F4" << endl;
	fo << F4 << endl;

	DaThuc F5 = *this * F2;
	//F5.RutGon();
	F5.ChuanHoa();
	fo << "F5" << endl;
	fo << F5 << endl;

	fo.close();
}

void main()
{
	DaThuc F1, F2, KQ;
	F1.Input("F1.txt");
	F2.Input("F2.txt");
	F1.Output("F-result.txt", F2);
	F1.RutGon();
	F2.RutGon();
	cout << "Phep cong:" << endl;
	KQ= F1 + F2;
	KQ.RutGon();
	KQ.ChuanHoa();
	cout << KQ;
	cout << "Phep Tru:" << endl;
	KQ.RutGon();
	KQ = F1 - F2;
	KQ.ChuanHoa();
	cout << KQ;
	cout << "Phep nhan:" << endl;
	KQ = F1 * F2;
	KQ.ChuanHoa();
	cout << KQ;
}


