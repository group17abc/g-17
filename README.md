# Thông Tin Cá Nhân # 

Đinh Văn Tuấn Hải		1512137
toihocly@gmail.com
## BÀI TẬP GIỮA KÌ ##
### Làm Được: ###
1.Thêm vào danh sách một mục chi, gồm 3 thành phần (3 điểm)


*Loại chi tiêu, nội dung chi, số tiền.


Ví dụ: Ăn uống, Ăn trưa, 15000.


Để đơn giản hóa, giả sử Loại chi tiêu chỉ gồm các loại sau đây:

+ Ăn uống (Sáng, trưa, chiều, xế, tối, khuya, trà sữa…)
+ Di chuyển (bus, đổ xăng, taxi, bơm xe, tàu…), 
+ Nhà cửa (tiền thuê nhà, nước, điện, nước lau nhà…), 
+ Xe cộ (đổ xăng, vá xe, bảo dưỡng, gởi xe…), 
+ Nhu yếu phẩm (xà bông, sữa tắm, dao cạo râu…), 
+ Dịch vụ (intenet, thẻ cào điện thoại…).

2.Xem lại danh sách các mục chi của mình, lưu và nạp vào tập tin text (4 điểm)

+ Khi chương trình chạy lên tự động nạp danh sách chi tiêu từ tập tin text lên và + hiển thị (gợi ý:  dùng Textbox ở chế độ multiline hoặc ListView)
+ Khi chương trình thoát thì tự động lưu danh sách mới vào tập tin text.
+ Chỉ cần lưu dạng không dấu là được.

3.Vẽ biểu đồ  nhằm biểu diễn trực quan tỉ lệ tiêu xài (2 điểm

### Làm Thêm: ###
1.Vẽ biểu đồ hình tròn (pie chart) để biểu thị tỉ lệ chi tiêu. (0.5 điểm)
2.Cải tiến về mặt trải nghiệm người dùng (UX – User experience) mà bạn tự nghĩ ra nhằm giúp việc sử dụng app hiệu quả hơn. (0.5 điểm)
### Chưa Làm Được: ###

Chưa xử lý được tiếng việt có dấu

### Bài Nộp gồm có ###
1. Mã nguồn.
2. Tập tin thực thi
3. Link youtube video demo [LINK YOUTUBE](https://youtu.be/hfWMedTRj1M)
4. Link repo trên bitbucket  [LINK BITBUCKET](https://bitbucket.org/1512137/windev.git)
5. File README.md + readme.docx

#### NOTE ####
Nếu vì trường hợp giáo viên bị lỗi link ở trên cảm phiền giáo viên gửi phản hồi để tôi có thể tìm ra cách khắc phục. Xin cảm ơn.
Gmail : toihocly@gmail.com

 Facebook: [www.facebook.com/tuanhai.dinhvan](www.facebook.com/tuanhai.dinhvan)