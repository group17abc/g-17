﻿#pragma once
#include<iostream>
using namespace std;

typedef struct DonThuc {//Là đơn thức có dạng a*x^n
	float a;
	unsigned int x;
	unsigned int n;
};

typedef struct PhanTu {//Một phần tử trong đa thức
	DonThuc data;// Lưu dữ liệu của một đơn thức
	PhanTu *Next;// Lưu con trỏ tới đơn thức kế tiếp trong đa thức.
	PhanTu* Tp; // Lưu các thành phần phụ lưu giá trị lưu các giá trị của biến số mà khi thực hiện phép nhân hai đơn thức có biên và mũ số khác nhau
};


class CDaThuc
{
	PhanTu *PHead;
	int n;
public:
	// Xuất nhập từ file 
	void Input(char* );
	void Output(char*);
	// Hình thức chuẩn hóa
	void RutGon();
	void ChuanHoa();

	//Các operator
	CDaThuc operator +(CDaThuc);
	CDaThuc operator -(CDaThuc);
	CDaThuc operator *(CDaThuc);

	// Các phương thức cần thiết
	bool IsEmpty(); // kiểm tra đa thức có rỗng hay không
	void Delete(int);// Xóa thành phần đơn thức thứ k đi
	void DeleteAll(); // Xóa luôn cái đa thức
	PhanTu *GetPhanTu(int); // Lấy ra phần tử thứ i
	void PutPhanTu(PhanTu *); // Thêm vào 1 phần tử vào cuối đa thức
	// Nếu thiếu thì thêm vào nhé !!!



	// Phương thức khởi tạo
	CDaThuc(string); // Tạo lập đa thức từ chuỗi string
	CDaThuc(PhanTu*); // Tạo lập đa thức từ việc lấy node đầu da thức
	CDaThuc();
	~CDaThuc();
};

